REMOTE_KEY_V1.1

ESP32-WROOM-32D

-   板载 W5500、HC595、STM32 芯片均与 ESP32 连接，详见下图 Layout 及板上丝印。

STM32F103C8T6

-   由于 STM32 的 `RESET` 和 `BOOT0` 被 ESP32 用于 ISP 升级,当使用 STLINK 开发调试 STM32 时，需避免被 ESP32 干扰

-   方法 1：保证 ESP32 的程序中 `IO16`、`IO17` 是悬空状态
-   方法 2：通过按住板载按钮【BOOT】不放再按板载按钮【RST】使 ESP32 进入下载等待状态，此时 `IO16`、`IO17` 自然也被释放了。【此时板载绿灯处于微亮状态！】
-   方法 3：使用ESP32的串口直接给STM32下载固件，这样可以省去STLINK，但需要熟悉STM32的ISP协议并移植到ESP32，github上有库。

![调试接口](调试口.jpg)

![Layout](Layout.jpg)
